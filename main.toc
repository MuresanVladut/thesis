\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}JavaScript as a standard}{2}{section.1.1}
\contentsline {chapter}{\numberline {2}Web Concepts}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}MVC}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}MVP}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}MVVM}{6}{section.2.3}
\contentsline {section}{\numberline {2.4}Single Page Applications}{7}{section.2.4}
\contentsline {section}{\numberline {2.5}Component Oriented Programming}{8}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Introduction}{8}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Components principles}{8}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}Web Components}{9}{subsection.2.5.3}
\contentsline {subsection}{\numberline {2.5.4}Web Components as a standard}{10}{subsection.2.5.4}
\contentsline {chapter}{\numberline {3}The evolution of JavaScript libraries and frameworks}{12}{chapter.3}
\contentsline {section}{\numberline {3.1}KnockoutJS}{13}{section.3.1}
\contentsline {section}{\numberline {3.2}BackboneJS}{13}{section.3.2}
\contentsline {section}{\numberline {3.3}AngularJS}{14}{section.3.3}
\contentsline {section}{\numberline {3.4}EmberJS}{15}{section.3.4}
\contentsline {section}{\numberline {3.5}ReactJS}{16}{section.3.5}
\contentsline {section}{\numberline {3.6}VueJS}{16}{section.3.6}
\contentsline {section}{\numberline {3.7}Angular}{17}{section.3.7}
\contentsline {section}{\numberline {3.8}Elm}{18}{section.3.8}
\contentsline {section}{\numberline {3.9}WebAssembly}{19}{section.3.9}
\contentsline {chapter}{\numberline {4}Frameworks in detail}{20}{chapter.4}
\contentsline {section}{\numberline {4.1}AngularJS}{20}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Modules}{21}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Controllers}{21}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Scope}{21}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Watch and Digest}{21}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Services, Factories, Providers}{22}{subsection.4.1.5}
\contentsline {section}{\numberline {4.2}ReactJS}{23}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Redux and Flux Architecture}{25}{subsection.4.2.1}
\contentsline {section}{\numberline {4.3}VueJS}{27}{section.4.3}
\contentsline {section}{\numberline {4.4}Angular}{30}{section.4.4}
\contentsline {chapter}{\numberline {5}social.com - Social publishing application}{34}{chapter.5}
\contentsline {section}{\numberline {5.1}Application Requirement}{34}{section.5.1}
\contentsline {section}{\numberline {5.2}Use Case Diagram}{35}{section.5.2}
\contentsline {section}{\numberline {5.3}Application Server}{37}{section.5.3}
\contentsline {section}{\numberline {5.4}Application Security}{39}{section.5.4}
\contentsline {section}{\numberline {5.5}Angular1 implementation}{40}{section.5.5}
\contentsline {section}{\numberline {5.6}Angular implementation}{42}{section.5.6}
\contentsline {section}{\numberline {5.7}ReactJS implementation}{43}{section.5.7}
\contentsline {section}{\numberline {5.8}VueJS implementation}{44}{section.5.8}
\contentsline {section}{\numberline {5.9}Performance Comparison}{45}{section.5.9}
\contentsline {section}{\numberline {5.10}Further implementation}{48}{section.5.10}
\contentsline {chapter}{\numberline {6}Conclusion}{49}{chapter.6}
